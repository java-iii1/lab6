package rpsgame;
import backend.RpsGame;
import java.util.Scanner;
/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        boolean game = true;
        RpsGame rg = new RpsGame();
        Scanner kb = new Scanner(System.in);
        String userChoice;
        while(game){
            System.out.println("Choose a option rock paper scissor");
            userChoice = kb.nextLine();
            rg.playRound(userChoice);
            System.out.println( rg.getWins() + rg.getLosses() + rg.getTies());
            System.out.println("Do you wanna continue?y/n");
            //kb.nextLine();
            userChoice = kb.nextLine();
            if(userChoice.equals("n")){
                game = false;
            }else 
                game = true;

        }
    }
}
