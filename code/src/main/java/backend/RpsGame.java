package backend;
import java.util.Random;
public class RpsGame {
 private int wins;
 private int ties;
 private int losses;
 private Random rand = new Random();
 private int randomNum = rand.nextInt(-1,4);

 public RpsGame(){
    this.wins = 0;
    this.ties = 0;
    this.losses = 0;
    this.randomNum = 0;
 }

 public String getWins(){
    return " Wins :" + this.wins;
 }
 public String getTies(){
    return " Ties :" + this.ties;
 }
 public String getLosses(){
    return " Losses :" + this.losses;
 }
 public String playRound(String choice){
 switch(choice){
    case "paper" :
        if(this.randomNum == 0){
            this.wins ++;
          return "Rock Player wins";
        } else if(this.randomNum ==1){
            this.losses ++;
           return "Scissors Computer wins" ;
        }
        else
            this.ties++;
            return "Paper Both player ties" ;
    case "scissors" :
    if(this.randomNum == 0){
        this.losses ++;
        return "Rock Computer wins" ;
    } else if(this.randomNum ==1){
        this.ties ++;
        return "Scissors Both player ties";
    }
    else
        this.wins++;
        return "Paper Player wins";
    case "rock" :
    if(this.randomNum == 0){
        this.ties ++;
        return "Rock Both player ties";
       
    } else if(this.randomNum ==1){
        this.wins ++;
        return "Scissors Player wins";
       
    }
    else
        this.losses++;
        return "Paper Computer wins";
       
        }  
        return "";
    }
   

 
}
